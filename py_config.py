import configparser
import os

def read_config(filename):
    if not os.path.exists(filename):
        cfg = configparser.ConfigParser()
        cfg.add_section('Ping')
        cfg.add_section('PortScan')
        cfg.set('Ping','address_scope','192.168.0')
        cfg.set('Ping', 'timeout', '3')
        cfg.set('PortScan', 'address', '192.168.0.1')
        cfg.set('PortScan', 'port_scope', '500-505')
        with open(filename, 'w') as configfile:
            cfg.write(configfile)
    config = configparser.ConfigParser()
    config.read(filename)
    return config

def write_config(config, filename):
    with open(filename, 'w') as configfile:
        config.write(configfile)