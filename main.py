# This is a sample Python script.

# Press Ctrl+R to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
import numpy as np

def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press F9 to toggle the breakpoint.


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    # 通过列表创建二维数组
    two_dim_array = np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9]])

    # 打印二维数组
    print("二维数组:")
    print(two_dim_array)
    print(two_dim_array.shape)

    from itertools import groupby

    # 示例数据
    data = [
        {'name': 'Alice', 'group': 'A'},
        {'name': 'Bob', 'group': 'B'},
        {'name': 'Charlie', 'group': 'A'},
        {'name': 'David', 'group': 'B'},
        {'name': 'Eve', 'group': 'A'},
    ]

    # 对数据按照 'group' 键进行排序（groupby 要求数据是有序的）
    sorted_data = sorted(data, key=lambda x: x['group'])

    # 使用 groupby 进行分组
    grouped_data = {key: list(group) for key, group in groupby(sorted_data, key=lambda x: x['group'])}

    # 打印分组结果
    for group, items in grouped_data.items():
        print(f"Group {group}: {items}")

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
