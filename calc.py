import sys
import cv2
import scipy

# pyinstaller -F --console  ./calc.py
if __name__=='__main__':
    # 获取命令行参数
    arguments = sys.argv

    # 输出命令行参数
    print("Number of arguments:", len(arguments))
    print("Argument list:", arguments)

    # 检查命令行参数数量
    if len(arguments) < 2:
        print("Usage: python script.py <arg1> <arg2> ...")
        sys.exit(1)

    # 输出单个命令行参数
    print("First argument:", arguments[1])
    print("Second argument:", arguments[2])
    print(int(arguments[1]) + int(arguments[2]))